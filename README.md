### How to start your LDE

__Pre-requirement:__ you had installed PostgreSQL, python 3.X with pip. 

Please check while you do not forget anything: 
1. Install pipenv https://docs.pipenv.org/install/#installing-pipenv
    
    $ pip install --user pipenv 
    
2. Install package for the project https://docs.pipenv.org/install/#installing-packages-for-your-project

    $ pipenv install 
    
3. Run django migrate to init database. https://docs.djangoproject.com/en/1.11/topics/migrations/

    $ python manage.py makemigrations   
    
4. Run collect static to prepare website static.

    $ python manage.py collectstatic 


5. Start django website

    $ python manage.py runserver
