# To build just type in cmd:
#	 docker build --tag vladymyr/goprint . 
# To test just type in cmd:
#    docker run -it --rm --entrypoint bash --name goprint-1 vladymyr/goprint
# To run just type in cmd:
#    docker run -d -p 8000:8000 --network=host --name goprint-daemon vladymyr/goprint 
#    docker run -d -p 8000:8000 --network=my-net --name goprint-daemon vladymyr/goprint
# To remove type in cmd:
#    docker conatiner ls
#    docker rm [#hash| name of container]
FROM python:3.6-slim-stretch

ADD . /app

RUN apt-get update &&\
    apt-get install --no-install-recommends -y gdal-bin gcc python3-dev musl-dev &&\
 	cd app &&\
	pip install -r requirements.txt &&\
	apt-get remove -y gcc python3-dev musl-dev  &&\
    apt autoremove -y &&\
    apt-get clean -y &&\
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


EXPOSE 8080
WORKDIR app
CMD gunicorn web.wsgi -p 8080 --log-file -
