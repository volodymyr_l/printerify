from django.contrib.gis import admin
from .models import PrintCenter, Order, Document, Feedback, Price

# Register your models here.
admin.site.register(PrintCenter, admin.OSMGeoAdmin)
admin.site.register(Price)
admin.site.register(Order)
admin.site.register(Document)
admin.site.register(Feedback)
