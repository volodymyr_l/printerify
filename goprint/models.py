from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.gis.db import models


class PrintCenter(models.Model):
    """Print center model."""
    address = models.CharField('Address', max_length=150)
    name = models.CharField('Name', max_length=50)
    point = models.PointField('Geo Coordinates')
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True,
                              related_name='User_Owner_PrintCenter')
    registered_at = models.DateTimeField('registration date', auto_now_add=True)

    @property
    def detail_url(self):
        return reverse('goprint:printcenter_detail', kwargs={'pk': self.id})

    @property
    def document_submit_url(self):
        return reverse('goprint:order_add', kwargs={'printcenter_id': self.id})

    def __str__(self):
        return "[" + self.name + "] " + self.address


class Document(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    file = models.FileField(upload_to='files/%Y/%m/%d')
    create_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        user_name = 'null' if self.user is None else self.user.username
        return str(self.file) + " [" + user_name + "]"


class Price(models.Model):
    print_center = models.ForeignKey(PrintCenter, on_delete=models.CASCADE, null=True)
    name = models.CharField('Service Name', max_length=200)
    price = models.DecimalField('Service Price', max_digits=4, decimal_places=2)
    edit_at = models.DateTimeField('last edit date', auto_now=True)

    def __str__(self):
        return self.name + " [" + str(self.price) + "]"


class Order(models.Model):
    document = models.ForeignKey(Document, on_delete=models.CASCADE)
    price = models.ForeignKey(Price, on_delete=models.CASCADE)
    create_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.create_at) + " [" + str(self.price) + "]"


class Feedback(models.Model):
    print_center = models.ForeignKey(PrintCenter, on_delete=models.CASCADE)
    message = models.CharField('Address', max_length=1000)
    rating = models.IntegerField('Rating')
