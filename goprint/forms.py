from django.contrib.gis import forms
from dal_select2 import widgets
from .models import PrintCenter, Order, Document


class PrintCenterForm(forms.ModelForm):
    class Meta:
        model = PrintCenter
        exclude = ('owner', 'registrator',)
        owner = forms.CharField(widget=forms.HiddenInput(), required=False)
        registrator = forms.CharField(widget=forms.HiddenInput(), required=False)
        widgets = {
            'point': forms.OSMWidget(
                attrs={
                    'map_width': 800,
                    'map_height': 500,
                    'default_lat': 49.93,
                    'default_lon': 36.18,
                }
            )
        }


class NaiveOrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = '__all__'


class OrderForm(forms.Form):
    document_file = forms.FileField()
    print_center = forms.CharField(widget=forms.HiddenInput())
    price = forms.CharField(
        widget=widgets.ListSelect2('goprint:price_cmplt',
        forward=('print_center',)))


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        exclude = ('user',)
