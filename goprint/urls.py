from django.urls import path
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path('printcenter.geojson', views.PrintCenterMapData.as_view(), name="printcenter_data"),

    path('print-center/add/', views.PrintCenterCreateView.as_view(), name='printcenter_add'),
    path('print-center/<int:pk>/prices/', views.PrintCenterPriceListView.as_view(), name='printcenter_prices'),
    path('print-center/<int:pk>/update/', views.PrintCenterUpdateView.as_view(), name='printcenter_update'),
    path('print-center/<int:pk>/', views.PrintCenterDetailView.as_view(), name='printcenter_detail'),
    path('print-center', views.PrintCenterListView.as_view(), name='printcenter_list'),

    path('order/add/<int:printcenter_id>/', views.OrderCreateView.as_view(), name='order_add'),
    path('order/<int:pk>/', views.OrderDetailView.as_view(), name='order_detail'),
    path('order/', views.OrderListView.as_view(), name='order_list'),

    path('print-centers-cmplt/', views.PrintCenterAutocomplete.as_view(), name='print_center_cmplt'),
    path('price-cmplt/', views.PriceAutocomplete.as_view(), name='price_cmplt'),

    path('<slug:city>/', views.PrintCenterMap.as_view(), name="printcenter_map"),
    path('', TemplateView.as_view(template_name='goprint/printcenter_landing.html')),
]

