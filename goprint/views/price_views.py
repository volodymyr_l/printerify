from dal import autocomplete
from django.views.generic import ListView

from goprint.models import Price


class PrintCenterPriceListView(ListView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['printcenter_pk'] = self.kwargs['pk']
        return context

    def get_queryset(self):
        return Price.objects.filter(print_center__pk=self.kwargs['pk'])


class PriceAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Price.objects.all()
        print_center = self.forwarded.get('print_center', None)

        if print_center:
            qs = qs.filter(print_center=print_center)

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs
