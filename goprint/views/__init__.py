from .order_view_set import OrderCreateView, OrderDetailView, OrderListView
from .price_views import PriceAutocomplete, PrintCenterPriceListView
from .print_center_views import PrintCenterMap, PrintCenterMapData, PrintCenterAutocomplete, PrintCenterCreateView, \
    PrintCenterListView, PrintCenterDetailView, PrintCenterUpdateView
