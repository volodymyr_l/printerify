from dal import autocomplete
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Prefetch
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.views.generic import FormView, ListView, DetailView, TemplateView, UpdateView
from djgeojson.views import GeoJSONLayerView

from goprint.forms import PrintCenterForm
from goprint.models import PrintCenter
from goprint.views.common import MapSettingsContextMixin, PrintCenterRoleContextMixin, PrintCenterGetterMixin


class PrintCenterMap(PrintCenterRoleContextMixin, MapSettingsContextMixin, LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('login')
    template_name = 'goprint/printcenter_map.html'


class PrintCenterMapData(LoginRequiredMixin, GeoJSONLayerView):
    login_url = reverse_lazy('login')
    model = PrintCenter
    geometry_field = 'point'
    properties = {'name': 'title', 'detail_url': 'detailUrl', 'document_submit_url': 'documentSubmitUrl'}


class PrintCenterCreateView(MapSettingsContextMixin, PrintCenterGetterMixin, LoginRequiredMixin, FormView):
    login_url = reverse_lazy('login')
    form_class = PrintCenterForm
    template_name = 'goprint/printcenter_create.html'
    user_printcenter = None

    def _redirect_to_detail(self):
        return HttpResponseRedirect(reverse('goprint:printcenter_detail', kwargs={"pk": self.user_printcenter.id}))

    def get(self, request, *args, **kwargs):
        self.user_printcenter = self.get_user_printcenter()
        if self.user_printcenter is not None:
            return self._redirect_to_detail()

        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        self.user_printcenter = self.get_user_printcenter()
        if self.user_printcenter is not None:
            return self._redirect_to_detail()

        model = form.save(commit=False)
        model.registrator = self.request.user
        model.owner = self.request.user
        model.address = form.cleaned_data['address']
        model.name = form.cleaned_data['name']
        model.point = form.cleaned_data['point']
        model.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('goprint:printcenter_list')


class PrintCenterUpdateView(MapSettingsContextMixin, PrintCenterGetterMixin, LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('login')
    form_class = PrintCenterForm
    model = PrintCenter
    template_name = 'goprint/printcenter_update.html'

    def _check_printcenter(self):
        return self.object.owner == self.request.user

    def get(self, request, *args, **kwargs):
        res = super().get(request, *args, **kwargs)
        if self._check_printcenter():
            return res
        return HttpResponseRedirect(reverse('goprint:printcenter_detail', kwargs=kwargs))

    def form_valid(self, form):
        model = form.save(commit=False)
        model.owner = self.request.user
        model.address = form.cleaned_data['address']
        model.name = form.cleaned_data['name']
        model.point = form.cleaned_data['point']
        model.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        kwargs['pk'] = self.kwargs['pk']
        return super().get_context_data(**kwargs)

    def get_success_url(self):
        return reverse_lazy('goprint:printcenter_detail', kwargs={'pk': self.kwargs['pk']})


class PrintCenterListView(PrintCenterRoleContextMixin, LoginRequiredMixin, ListView):
    login_url = reverse_lazy('login')
    model = PrintCenter


class PrintCenterDetailView(MapSettingsContextMixin, PrintCenterRoleContextMixin, LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('login')
    form = PrintCenterForm

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        map_settings = {
            'DEFAULT_ZOOM': 16,
            'DEFAULT_CENTER': self.object.point.tuple[::-1]
        }
        context = self.get_context_data(object=self.object, map_settings=map_settings)
        return self.render_to_response(context)

    def get_queryset(self):
        queryset = PrintCenter.objects.all()
        PrintCenter.objects.prefetch_related(Prefetch('price_set', queryset=queryset))
        return queryset


class PrintCenterAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    login_url = reverse_lazy('login')

    def get_queryset(self):
        qs = PrintCenter.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs
