from django.db.models import Prefetch
from django.urls import reverse
from django.views.generic import FormView, ListView, DetailView

from goprint.forms import OrderForm
from goprint.models import Document, Price, Order
from goprint.views.common import MapSettingsContextMixin


class OrderCreateView(FormView):
    form_class = OrderForm
    template_name = 'goprint/order_create.html'

    def get_success_url(self):
        return reverse('goprint:order_list')

    def form_valid(self, form):
        document = Document.objects.create(file=form.cleaned_data['document_file'], user=self.request.user)
        document.save()
        price = Price.objects.get(id=form.cleaned_data['price'])
        if price.print_center.id != self.kwargs['printcenter_id']:
            raise Exception('incorrect princenter_id')

        order = Order.objects.create(document=document, price=price)
        order.save()
        return super().form_valid(form)

    def get_initial(self):
        initial = super().get_initial()
        initial['print_center'] = self.kwargs['printcenter_id']

        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['printcenter_id'] = self.kwargs['printcenter_id']
        return context


class OrderListView(MapSettingsContextMixin, ListView):
    def get_queryset(self):
        return Order.objects.prefetch_related('price')\
            .filter(document__user=self.request.user)


class OrderDetailView(MapSettingsContextMixin, DetailView):
    model = Order
