from goprint.models import PrintCenter

city_map_settings = {
    'kharkiv': {'DEFAULT_CENTER': (49.9898255, 36.2312968), 'DEFAULT_ZOOM': 13},
    'kyiv': {'DEFAULT_CENTER': (50.4387008, 30.4778504), 'DEFAULT_ZOOM': 12}
}


class MapSettingsContextMixin:
    def get_context_data(self, **kwargs):
        context = {}
        if 'city' in kwargs:
            self.request.session['city'] = kwargs['city'].lower()

        city_name = context['city'] = self.request.session['city']
        if city_name is None:
            raise ValueError('"city" not defined')

        context['map_settings'] = kwargs['map_settings'] if 'map_settings' in kwargs else {}
        if city_name in city_map_settings:
            context['map_settings'] = {**city_map_settings[city_name], **context['map_settings']}
            if 'map_settings' in kwargs:
                del kwargs['map_settings']

        context.update(kwargs)
        return super().get_context_data(**context)


class PrintCenterGetterMixin:
    def get_user_printcenter(self):
        return PrintCenter.objects \
            .order_by('registered_at') \
            .filter(owner=self.request.user) \
            .first()


class PrintCenterRoleContextMixin(PrintCenterGetterMixin):
    def get(self, request, *args, **kwargs):
        if self.extra_context is None:
            self.extra_context = {}

        # TODO: use values() method to get only few columns
        self.extra_context['user_printcenter'] = self.get_user_printcenter()

        return super().get(request, *args, **kwargs)
