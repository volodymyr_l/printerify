from django.urls import include, path
from django.contrib import admin
from django.contrib.auth import views as auth_views


admin.autodiscover()
urlpatterns = [
    path('accounts/', include('registration.backends.hmac.urls')),
    path('accounts/login', auth_views.LoginView.as_view(), name='login'),
    path('accounts/logout', auth_views.LogoutView.as_view(), name='logout'),
    path('accounts/', include('registration.backends.hmac.urls')),
    # url(r'^admin/', include(admin.site.urls)),
    # url(r'^social/', include('social_django.urls', namespace='social')),
    path('admin/', admin.site.urls),
    path('', include(('goprint.urls', 'goprint'), namespace='goprint')),
]
