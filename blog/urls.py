from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'(?P<id>[0-9]+)/?$', views.PostRetriveUpdateDestroy.as_view(),
        name='PostRetriveUpdateDestroy'),
    url(r'$', views.PostList.as_view(), name='PostList'),
]